console.log("Hello World"); 


// 3. Create an addStudent() function that will accept a name of the student and add it to the student array.
let students = [];
function addStudent(name){
	students.push(name);
	console.log(`${name} was added to the student's list.`)
}

// 4. Create a countStudents() function that will print the total number of students in the array.

function countStudents(){
	console.log(`There are a total of ${students.length} students enrolled.`)
}


// 5. Create a printStudents() function that will sort and individually print the students (sort and forEach methods) in the array.
function printStudents(){
	students.sort();
	students.forEach(function(student){
		console.log(student)
	})

}



// 6. Create a findStudent() function that will do the following:
// Search for a student name when a keyword is given (filter method).
// - If one match is found print the message studentName is an enrollee.
// - If multiple matches are found print the message studentNames are enrollees.
// - If no match is found print the message studentName is not an enrollee.
// - The keyword given should not be case sensitive.


function findStudent(keyword) {

	let match = students.filter(function(student) {
		return student.toLowerCase().includes(keyword.toLowerCase())
	})

	if(match.length == 1){
		console.log( `${match} is an Enrollee`);
	} else if(match.length > 1){
		console.log(`${match} are Enrollees`);
	} else {
		console.log(`No student found with the name ${keyword}`);
	}
}






// let fruitsLength = fruits.push('mango');







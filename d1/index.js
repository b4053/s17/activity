console.log("Hello World"); 

// JAVASCRIPT - ARRAY MANIPULATION
// store multiple values in a single variable
//[] --> array literals


//common examples of array
let grades = [98.5, 91.2, 93.1, 89.0];
console.log(grades[0]);


//alternative way to write arrays
let mytasks = [
	'drink html',
	'eat javascript',
	'inhale css',
	'bake express js',
];


//reassigning array values
console.log('array before reassignment');
console.log(mytasks)

mytasks[0] = 'hello world';
console.log('array before reassignment');
console.log(mytasks);

//accessing an array element but not exist will return "undefined"
console.log(mytasks[4]);

//getting the length of an array
let computerBrands = ['acer', 'asus', 'lenovo', 'neo', 'toshiba', 'gateway', 'redfox', 'fujitsu']
console.log(computerBrands.length); //length
console.log(computerBrands); //index -1 coz its starts from 0 

if(computerBrands.length > 5){
	console.log('We have too many suppliers. Please coordinate with the operation manager.');
}

//access the last element of an array
let lastElementIndex = computerBrands.length -1;
console.log(computerBrands[lastElementIndex]);

//array methods
//mutator methods - mutate or change an array after ther're created like adding element, remove an element

let fruits = ['apple', 'orange', 'kiwi', 'dragon fruit'];

//push()
//adds an element in the of an array returns arrays length. pinakalast napiprint
//syntax: arrayName.push();

console.log('current array:');
console.log(fruits);
let fruitsLength = fruits.push('mango');
console.log(fruitsLength);
console.log('Mutated array from push method');
console.log(fruits)

//add elements
fruits.push('avocado', 'guava')
console.log('Mutated array from pop method');
console.log(fruits)

//pop()
//removes the last elements in an array and can also returns the removed element. pinakalast nareremove
//arrayName.pop()

let removedFruit = fruits.pop();
console.log(removedFruit);
console.log(fruits);

fruits.pop()
console.log(fruits);

//unshift()
//adds one more elements at the beginning of an array
//arrayName.unshift('elementA', 'elementB');

fruits.unshift('lime', 'banana');
console.log('Mutated array from unshift method');
console.log(fruits);

//shift()
//removes an element from the beginning and returns the removed element
//arrayName.shift();

let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log(fruits);

//splice()
//simultaneously removes element from a specified index number and adds elements
//arrayName.splice(startingIndex, deleteCount, elementsToBeAdded);

fruits.splice(1, 2, 'lime', 'cherry');
console.log('splice method');
console.log(fruits)

//sort()
//rearranges the array elements in alphanumeric order
//arrayName.sort();

fruits.sort();
console.log('sort method');
console.log(fruits);

//reverse()
//reverses the order
//arrayName.revers();


fruits.reverse();
console.log('reverse method');
console.log(fruits);


//Non-Mutator Methods
//do not change the array, cannot modify and manipulate the arrays. filtering elements in an array, returns and combining array and printing the output

let countries = ['US', 'PH', 'CA', 'SG', 'TH', 'PH', 'FR', 'DE'];

//indexOf()
//returns the index number of the first matching elements found in an array
//if no match was found, the result will be -1
//the search process will be done from the first element proceeding to the last element
//arrayName.indexOf(serchValue)
//arrayName.indexOf(serchValue, fromIndex) kung saan magstart magsearch

let firstIndex = countries.indexOf('PH');
console.log(`result of indexOf method: ${firstIndex}`);

let invalidCountry = countries.indexOf('BR');
console.log(`indexOf: ${invalidCountry}`);

//lastIndexOf()
//returns the index number of the last matching element fund in an array
//search preocess id from the last to first
//arrayName.lastIndexOf(searchValue)

let lastIndex = countries.lastIndexOf('PH');
console.log(`result of lastIndexOf method: ${lastIndex}`);

//getting the index number starting from an specified index

let lastIndexStart = countries.lastIndexOf('PH', 4);
console.log(`lastIndexOf: ${lastIndexStart}`);

//slice()
//portions/slices of elements and returns new array
//arrayname.slice(startingIndex);
//arrayname.slice(startingIndex, endingIndex);

//slicing of elements from a specified index to the last element
let slicedArrayA = countries.slice(2);//saan sisismulan islice
console.log(countries);
console.log('slice method:');
console.log(slicedArrayA);

//slicing of elements starting from a specified index to the another element
let slicedArrayB = countries.slice(2,4);
console.log(slicedArrayB);

//starting from the last element of an array
let slicedArrayC = countries.slice(-3);
console.log(slicedArrayC);

//toString()
//return array as a string, separated by commas

let stringArray = countries.toString();
console.log('toString method:');
console.log(stringArray);

//concat()
//combine two arrays and return the combined result
//arrayA.concat(arrayB);
//arrayA.concat(elementA);

let tasksArrayA = ['drink html', 'eat javascript'];
let tasksArrayB = ['inhale css', 'breath sass'];
let tasksArrayC = ['get git', 'be node'];
let tasks = tasksArrayA.concat(tasksArrayB);
console.log(`concat method: ${tasks}`)

//combining multiple arrays
let allTasks = tasksArrayA.concat(tasksArrayB, tasksArrayC);
console.log(allTasks);

//combine arrays with element
let combinedTasks = tasksArrayA.concat('smell express', 'throw react');
console.log(combinedTasks);

//join()
//return array as a string, separated by specified separator
// arrayName.join('separatorString')

let users = ['john', 'jane', 'joe', 'robert']
console.log(users.join());
console.log(users.join(' '));
console.log(users.join(' - '));


//iteration methods
//loops design to perform repetitive task on arrays


//forEach()
//iterate each element in the array
//wotk with a function supplied as an argument
/*arrayName.forEach(fucntion(indivElement){
	statements
})
*/

allTasks.forEach(function(task){
	console.log(task)
});
 

// miniactivity
for (i = 0; i<allTasks.length; i++){
	console.log(allTasks[i]);
}

//using forEach with conditional statements
let filteredTasks = [];
allTasks.forEach(function(task){
	if (task.length > 10){
		filteredTasks.push(task);
	}
})
console.log('result of filteredTasks:')
console.log(filteredTasks)


//map()
//iterates on each element and returns new array with diff values depending on the results of the function's operation
//requires the use of return statement
/*	let/const resultArray = ArrayName.map(function(indivElement){
		return statement
	})
*/

let numbers = [1, 2, 3, 4, 5];
let numberMap = numbers.map(function(number){
	return number * number
})
console.log('Map method:');
console.log(numbers);
console.log(numberMap);


//every() &&
/*iterate the whole array, checks if all elements meet the given condition. 
Validating data stored in arrays especially when dealing with large amouns of data.
Returns true if all elements meet the condition and false if otherwise.
let/const resultArray = arrayName(function(indivElement){
	return expression/condition
})
*/

let allValid = numbers.every(function(number){
	return (number > 0);
})
console.log('every method:');
console.log(allValid);


//some() ||
/*check if atleast one element meets the condition and return a boolean value
*/

let someValid = numbers.some(function(number){
 	return (number < 3);
})
console.log('some method:');
console.log(someValid);

// combining the returned result from the every or the some method maby used in other statement (if else) to perform consecutive results
if (someValid){
	console.log('Some of numbers in the array are greater than 2');
}

if(allValid){
	console.log('ALL of numbers in the array are greater than 0');
}



//filter()
/*return new array that contains elements which meets the given condition
returns an empty array if no elements were found
let/const resultArray = arrayName.filter(function(indivElem){
	return expression/condition
})
*/

let filterValid = numbers.filter(function(number){
	return (number < 3);
})
console.log('filter method:');
console.log(filterValid);

let nothingFound = numbers.filter(function(number){
	return (number==0);
})
console.log(nothingFound);


//filtering using for each
let filteredNumber = [];
numbers.forEach(function(number){
	if(number<3){
		filteredNumber.push(number);
	}
})
console.log(filteredNumber);

//another example using the filter

let products = ['mouse', 'keyboard', 'laptop', 'monitor'];
let filteredProducts = products.filter(function(product){
	return product.toLowerCase().includes('a') //chained methods
})
console.log(filteredProducts);


//reduce method
/*evaluates the elements from left to right,


*/


//multidimensional arrays
//for complex data structures


let chessBoard = [
	['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
	['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
	['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
	['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
	['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],

];
console.log(chessBoard);
console.log(chessBoard[0][2]);
console.log('pawn moves to:' + chessBoard[1][5]);




















